<?php
require "database.php";


if (array_key_exists('_method', $_POST) && $_POST['_method'] === 'POST'){
    $_POST['product_image'] = saveImage();
    createProduct($_POST);
} else if (array_key_exists('id', $_GET) && array_key_exists('_method', $_POST) && $_POST['_method'] === 'PATCH'){
    $_POST['product_image'] = saveImage();
    if ($_POST['product_image_old'] !== $_POST['product_image']) {
        unlink($_POST['product_image_old']);
    }
    editProduct($_GET['id'], $_POST);
}

function saveImage() {
    $uploadsDir = 'dist/img/products/';
    $fileName = $uploadsDir . basename($_FILES['product_image']['name']);
    if (!file_exists($fileName)) {
        move_uploaded_file($_FILES['product_image']['tmp_name'], $fileName);
    }
    return $fileName;
}

header('Location: http://localhost/final_project/');