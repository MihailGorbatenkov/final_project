<?php
$host = 'localhost';
$user = 'root';
$pwd = '';
$db = 'final_project';

function connect() {
    global $host, $user, $pwd, $db;
    $connection = mysqli_connect($host, $user, $pwd, $db);
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }
    
    return $connection;
}

function selectProducts(array $parameters = []): array
{
    $connection = connect();
    $sql = "SELECT * FROM products";
    $stmt = mysqli_stmt_init($connection);
    $conditions = [];
    $paramValues = [];
    $bindTypes = '';
    if (array_key_exists('category', $parameters)) {
        $conditions[] = ' category = ? ';
        $paramValues[] = &$parameters['category'];
        $bindTypes .= 's';
    }
    if (array_key_exists('price_gte', $parameters)) {
        $conditions[] = ' price >= ? ';
        $paramValues[] = &$parameters['price_gte'];
        $bindTypes .= 'd';
    }
    if (array_key_exists('price_lte', $parameters)) {
        $conditions[] = ' price <= ? ';
        $paramValues[] = &$parameters['price_lte'];
        $bindTypes .= 'd';
    }

    if (array_key_exists('table_search', $parameters)) {
        if (is_numeric($parameters['table_search'])) {
            $conditions[] = ' id = ? ';
            $idSearch = intval($parameters['table_search']);
            $paramValues[] = &$idSearch;            
            $bindTypes .= 'd';
        }
        $conditions[] = " name LIKE ? ";
        $textSearch = "%" . $parameters['table_search'] . "%";
        $paramValues[] = &$textSearch;
        $bindTypes .= 's';
    }

    if (count($conditions) > 0) {
        if (array_key_exists('table_search', $parameters)) {
            $sql .= ' WHERE ' . implode('or', $conditions);
        } else {
            $sql .= ' WHERE ' . implode('and', $conditions);
        }        
    }
    mysqli_stmt_prepare($stmt, $sql);
    if (count($conditions) > 0) {
        $arguments = array_merge([$stmt, $bindTypes], $paramValues);
        call_user_func_array('mysqli_stmt_bind_param', $arguments);
    }
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $data =[];

    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
          array_push($data, $row);
        }
      }

    return $data;
}

function selectProduct(int $id) : array
{
    $connection = connect();
    $sql = "SELECT * FROM products WHERE id=?";
    $stmt = mysqli_stmt_init($connection);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "i", $id);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $data = mysqli_fetch_assoc($result);
    mysqli_stmt_close($stmt);
    mysqli_close($connection);

    return $data;
}

function createProduct(array $product)
{
    $connection = connect();
    $sql = "INSERT INTO products (name, image_path, category, price, discount, quantity, description) VALUES (?,?,?,?,?,?,?)";
    $stmt = mysqli_stmt_init($connection);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "sssddis", 
        $product['product_name'],
        $product['product_image'],
        $product['category'],
        $product['price'],
        $product['discount'],
        $product['available_quantity'],
        $product['description']
    );
    mysqli_stmt_execute($stmt);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);
    mysqli_close($connection);
}

function editProduct(int $id, array $product)
{
    $connection = connect();
    $sql = "UPDATE products set name=?, image_path=?, category=?, price=?, discount=?, quantity=?, description=? WHERE id=?";
    $stmt = mysqli_stmt_init($connection);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "sssddisi", 
        $product['product_name'],
        $product['product_image'],
        $product['category'],
        $product['price'],
        $product['discount'],
        $product['available_quantity'],
        $product['description'],
        $id
    );
    mysqli_stmt_execute($stmt);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);
    mysqli_close($connection);
}

function deleteProduct(int $id)
{
    $connection = connect();
    $sql = "DELETE FROM products WHERE id=?";
    $stmt = mysqli_stmt_init($connection);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "i", $id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    mysqli_close($connection);
}
