<?php
$products = [
    [
        'id' => '1',
        'image_path' => './dist/img/products/shoe-1.jpeg',
        'name' => 'Nike Series 2',
        'category' => 'Sneakers',
        'price' => '25',
        'discount' => '2',
        'quantity' => '30',
        'description' => 'Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.'
    ],
    [
        'id' => '2',
        'image_path' => './dist/img/products/shoe-2.jpg',
        'name' => 'Nike Air 3',
        'category' => 'Sneakers',
        'price' => '39',
        'discount' => '7',
        'quantity' => '39',
        'description' => 'Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.'
    ],
    [
        'id' => '3',
        'image_path' => './dist/img/products/shoe-3.jpeg',
        'name' => 'Nike Running S-25',
        'category' => 'Running Shoes',
        'price' => '50',
        'discount' => '6',
        'quantity' => '36',
        'description' => 'Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.'
    ],
    [
        'id' => '4',
        'image_path' => './dist/img/products/shoe-4.jpg',
        'name' => 'Nike Running S-28',
        'category' => 'Running Shoes',
        'price' => '55',
        'discount' => '8',
        'quantity' => '25',
        'description' => 'Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.'
    ],
    [
        'id' => '5',
        'image_path' => './dist/img/products/shoe-5.jpeg',
        'name' => 'Nike Air Jordan 5',
        'category' => 'Basketball Shoes',
        'price' => '48',
        'discount' => '0',
        'quantity' => '23',
        'description' => 'Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.'
    ],
    [
        'id' => '6',
        'image_path' => './dist/img/products/shoe-6.jpg',
        'name' => 'Nike Easywalk 2',
        'category' => 'Walk Shoes',
        'price' => '22',
        'discount' => '0',
        'quantity' => '29',
        'description' => 'Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.'
    ],
    [
        'id' => '7',
        'image_path' => './dist/img/products/shoe-7.jpg',
        'name' => 'Nike Go 2',
        'category' => 'Sneakers',
        'price' => '35',
        'discount' => '0',
        'quantity' => '10',
        'description' => 'Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.'
    ],
    [
        'id' => '8',
        'image_path' => './dist/img/products/shoe-8.jpg',
        'name' => 'Nike Pegasus 23',
        'category' => 'Running Shoes',
        'price' => '65',
        'discount' => '0',
        'quantity' => '65',
        'description' => 'Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.'
    ]
];
$parameters = [];
require "database.php";
if (!empty($_GET['category'])) {
    $parameters['category'] = $_GET['category'];
}
if (!empty($_GET['price_range'])) {
    $priceRange = explode('_', $_GET['price_range']);
    if (array_key_exists(0, $priceRange)) {
        $parameters['price_gte'] = $priceRange[0];
    }
    if (array_key_exists(1, $priceRange) && (int)$priceRange[1] > 0) {
        $parameters['price_lte'] = $priceRange[1];
    }
}

if (!empty($_GET['table_search'])) {
    $parameters['table_search'] = $_GET['table_search'];
}

$products = selectProducts($parameters);

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Nike Shoe Market</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!--Our custom styles-->
    <link rel="stylesheet" href="dist/css/custom.css">
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- SEARCH FORM -->
            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
                    </div>
                </div>
            </form>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="/final_project" class="brand-link">
                <img src="dist/img/AdminLTELogo.png" alt="Deposit Management Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">Deposit Management</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="/final_project" class="d-block">Main Admin</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <a href="index.html" class="nav-link">
                                <i class="nav-icon fa fa-archive" aria-hidden="true"></i>
                                <p>
                                    Products List
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Products List</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Products List</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Nike Shoes</h3>
                                    <div class="card-tools">
                                        <form action="" method="get">
                                            <div class="input-group input-group-sm" style="width: 150px;">
                                                <input type="number|text" name="table_search" class="form-control float-right" placeholder="Search">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="d-inline-flex float-right col-xs-1">
                                        <button 
                                            type="button" 
                                            class="btn btn-block btn-primary table-filters" 
                                            data-toggle="collapse" 
                                            data-target="#filters" 
                                            aria-expanded="false" 
                                            aria-controls="filters">
                                            Filters
                                        </button>
                                    </div>
                                    <div class="d-inline-flex float-right col-xs-1">
                                        <a 
                                            type="button" 
                                            class="btn btn-block btn-success table-filters" 
                                            href="create.php"
                                        >
                                            Create
                                        </a>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="collapse" id="filters">
                                    <div class="card-body">
                                        <form class="row">
                                            <div class="form-group col-3">
                                                <label>Category</label>
                                                <select class="form-control" name="category">
                                                    <option value=''>All</option>
                                                    <option value="Sneakers"<?php if(!empty($_GET['category']) && $_GET['category'] === 'Sneakers') echo 'selected' ?>>Sneakers</option>
                                                    <option value="Running Shoes"<?php if(!empty($_GET['category']) && $_GET['category'] === 'Running Shoes') echo 'selected' ?>>Running Shoes</option>
                                                    <option value="Basketball Shoes"<?php if(!empty($_GET['category']) && $_GET['category'] === 'Basketball Shoes') echo 'selected' ?>>Basketball Shoes</option>
                                                    <option value="Walking Shoes"<?php if(!empty($_GET['category']) && $_GET['category'] === 'Walking Shoes') echo 'selected' ?>>Walking Shoes</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-3">
                                                <label>Price</label>
                                                <select class="form-control" name='price_range'>
                                                    <option value=''>All</option>
                                                    <option value="10_20" <?php if(!empty($_GET['price_range']) && $_GET['price_range'] === '10_20') echo 'selected' ?>>10-20$</option>
                                                    <option value="21_30" <?php if(!empty($_GET['price_range']) && $_GET['price_range'] === '21_30') echo 'selected' ?>>21-30$</option>
                                                    <option value="31_40" <?php if(!empty($_GET['price_range']) && $_GET['price_range'] === '31_40') echo 'selected' ?>>31-40$</option>
                                                    <option value="41_50" <?php if(!empty($_GET['price_range']) && $_GET['price_range'] === '41_50') echo 'selected' ?>>41-50$</option>
                                                    <option value="50_" <?php if(!empty($_GET['price_range']) && $_GET['price_range'] === '50_') echo 'selected' ?>>50$+</option>
                                                </select>
                                            </div>
                                            <div class="form-group align-self-end col-1">
                                                <button type="submit" class="btn btn-block btn-primary">Filter</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-head-fixed text-nowrap table-striped product-list-table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>ID</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Price</th>
                                                <th>Sale Price</th>
                                                <th>Available Quantity</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($products as $product):?>
                                            <tr>
                                                <td>
                                                    <a class="mr-2" href="create.php?id=<?php echo $product['id']?>"><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="delete.php?id=<?= $product['id']?>&_method=DELETE"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                                <td><?php echo $product['id']?></td>
                                                <td><img src="<?= $product['image_path']?>"/></td>
                                                <td><?= $product['name']?></td>
                                                <td><?= $product['category']?></td>
                                                <td><?= $product['price']?>$</td>
                                                <td><?= $product['price'] - $product['discount']?>$</td>
                                                <td><?= $product['quantity']?> pairs</td>
                                                <td><?= $product['description']?></td>
                                            </tr> 
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <ul class="pagination pagination-sm m-0 float-right">
                                        <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                Anything you want
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
</body>

</html>